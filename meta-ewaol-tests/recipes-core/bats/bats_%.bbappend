SRC_URI = "git://github.com/bats-core/bats-core.git;branch=master;protocol=https"

# v1.8.2
SRCREV = "e8c840b58f0833e23461c682655fe540aa923f85"

PV = "1.8.2"
