# Copyright (c) 2022-2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "SOAFEE Test Suite"
DESCRIPTION = "SOAFEE compliance test to determine whether a software stack is"
DESCRIPTION += "SOAFEE compliant or not."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/soafee/soafee-test-suite;protocol=https;branch=stable;"
SRCREV = "f65c14d292dc41cd8b47593c6e8e1cf261259742"

S = "${WORKDIR}/git"

inherit allarch

do_patch[noexec] = "1"
do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {
    oe_runmake PREFIX=${D}/${prefix} install
}

RDEPENDS:${PN} += "bats"
RDEPENDS:${PN} += "docker"
RDEPENDS:${PN} += "ltp"
RDEPENDS:${PN} += "stress-ng"
